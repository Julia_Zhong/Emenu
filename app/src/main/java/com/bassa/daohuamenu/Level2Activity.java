package com.bassa.daohuamenu;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class Level2Activity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private TextView categoryName;
    private GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level2);

        Intent intent = getIntent();
        String name = intent.getStringExtra("category");
        categoryName = (TextView) findViewById(R.id.categoryName);
        categoryName.setText(name);

        gridView = (GridView) findViewById(R.id.gridView);
        gridView.setAdapter(new Level2Adapter(this));
        gridView.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent level3Intent = new Intent(this, Level3Activity.class);

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        Meal meal = (Meal) viewHolder.getMealImage().getTag();

        level3Intent.putExtra("imageId", meal.getImageId());
        level3Intent.putExtra("name", meal.getName());
        level3Intent.putExtra("description", meal.getDescription());

        startActivity(level3Intent);

    }

}

class Meal {

    private int imageId;
    private String name;
    private String description;

    public Meal(int imageId, String name, String description) {
        this.imageId = imageId;
        this.name = name;
        this.description = description;
    }

    public int getImageId() {
        return imageId;
    }

    public String getName() { //
        return name; //
    }

    public String getDescription() { //
        return description; //
    }

}

class ViewHolder {

    private ImageView mealImage;
    private TextView mealName; //
    private TextView mealDescription; //

    public ViewHolder(View v) {
        mealImage = (ImageView) v.findViewById(R.id.imageView);
        mealName = (TextView) v.findViewById(R.id.mealNameTextView); //
        mealDescription = (TextView) v.findViewById(R.id.mealDescriptionTextView); //
    }

    public ImageView getMealImage() {
        return mealImage;
    }

    public TextView getMealName() { //
        return mealName; //
    }

    public TextView getMealDescription() { //
        return mealDescription; //
    }

}

class Level2Adapter extends BaseAdapter {

    private ArrayList<Meal> meals;
    private Context context;

    public Level2Adapter(Context context) {

        this.context = context;

        meals = new ArrayList<Meal>();

        Resources resources = context.getResources();
        String[] mealNames = resources.getStringArray(R.array.meal_names);
        String[] mealDescriptions = resources.getStringArray(R.array.meal_descriptions);

        int[] mealImageIds = {R.drawable.meal_1, R.drawable.meal_2, R.drawable.meal_3, R.drawable.meal_4,
        R.drawable.meal_5, R.drawable.meal_6, R.drawable.meal_7, R.drawable.meal_8};

        for (int i = 0; i < mealNames.length; i++) {
            meals.add(new Meal(mealImageIds[i], mealNames[i], mealDescriptions[i]));
        }

    }

    @Override
    public int getCount() {
        return meals.size();
    }

    @Override
    public Object getItem(int position) {
        return meals.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        ViewHolder viewHolder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.level2_activity_single_meal, parent, false);
            viewHolder = new ViewHolder(row);
            row.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        Meal meal = meals.get(position);

        viewHolder.getMealName().setText(meal.getName()); //
        viewHolder.getMealDescription().setText(meal.getDescription()); //

        viewHolder.getMealImage().setImageResource(meal.getImageId());
        viewHolder.getMealImage().setTag(meal);

        return row;

    }

}
