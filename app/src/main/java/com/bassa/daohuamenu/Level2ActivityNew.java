package com.bassa.daohuamenu;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Level2ActivityNew extends AppCompatActivity {

    private TextView categoryName;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level2_new);

        Intent intent = getIntent();
        String category = intent.getStringExtra("category");
        categoryName = (TextView) findViewById(R.id.categoryName);
        categoryName.setText(category);

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(new Level2AdapterNew(this, category));

    }

}

class MealNew {

    private boolean sectionHeader;
    private String sectionHeaderName;

    private String ID;
    private String nameEst;
    private String nameRus;
    private String nameEng;
    private String nameCh;
    private String quantity; // only for drinks
    private String price;

    public MealNew(String ID, String nameEst, String nameRus, String nameEng, String nameCh, String quantity, String price) {

        this.sectionHeader = false;

        this.ID = ID;
        this.nameEst = nameEst;
        this.nameRus = nameRus;
        this.nameEng = nameEng;
        this.nameCh = nameCh;
        this.quantity = quantity;
        this.price = price;

    }

    public MealNew(String sectionHeaderName) {
        this.sectionHeader = true;
        this.sectionHeaderName = sectionHeaderName;
    }

    public boolean isSectionHeader() {
        return sectionHeader;
    }

    public String getSectionHeaderName() {
        return sectionHeaderName;
    }

    public String getID() {
        return ID;
    }

    public String getNameEst() {
        return nameEst;
    }

    public String getNameRus() {
        return nameRus;
    }

    public String getNameEng() {
        return nameEng;
    }

    public String getNameCh() {
        return nameCh;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getPrice() {
        return price;
    }

}

class ViewHolderNew {

    private TextView firstRow;
    private TextView secondRow;
    private TextView thirdRow;
    private TextView fourthRow;
    private TextView price;

    private TextView sectionHeader;

    public ViewHolderNew(View v) {

        firstRow = (TextView) v.findViewById(R.id.firstTextView);
        secondRow = (TextView) v.findViewById(R.id.secondTextView);
        thirdRow = (TextView) v.findViewById(R.id.thirdTextView);
        fourthRow = (TextView) v.findViewById(R.id.fourthTextView);
        price = (TextView) v.findViewById(R.id.priceTextView);

        sectionHeader = (TextView) v.findViewById(R.id.sectionHeaderTextView);

    }

    public TextView getFirstRow() {
        return firstRow;
    }

    public TextView getSecondRow() {
        return secondRow;
    }

    public TextView getThirdRow() {
        return thirdRow;
    }

    public TextView getFourthRow() {
        return fourthRow;
    }

    public TextView getPrice() {
        return price;
    }

    public TextView getSectionHeader() {
        return sectionHeader;
    }
}

class Level2AdapterNew extends BaseAdapter {

    private static final int ITEM = 0;
    private static final int SEPARATOR = 1;

    private ArrayList<MealNew> meals;

    private Context context;
    private String category;

    public Level2AdapterNew(Context context, String category) {

        this.context = context;
        this.category = category;

        meals = new ArrayList<>();

        // let's populate the "meals" array with all the section headers and all the meals we have

        Resources resources = context.getResources();

        String[] sectionHeaderNames = new String[0];
        int[] nrOfItemsInSectionHeaders = new int[0];

        String[] IDs = new String[0];
        String[] namesEst = new String[0];
        String[] namesRus = new String[0];
        String[] namesEng = new String[0];
        String[] namesCh = new String[0];
        String[] drinkQuantity = new String[0];
        String[] prices = new String[0];

        if (category.equals("Joogid")) {

            sectionHeaderNames = resources.getStringArray(R.array.drinks_sectionheaders);
            nrOfItemsInSectionHeaders = resources.getIntArray(R.array.nr_of_items_in_drinks_sectionheaders);

            namesEst = resources.getStringArray(R.array.drink_est);
            namesEng = resources.getStringArray(R.array.drink_eng);
            drinkQuantity = resources.getStringArray(R.array.drink_quantity);
            prices = resources.getStringArray(R.array.drink_prices);

            int n = 0; // drink we are currently on
            for (int i = 0; i < sectionHeaderNames.length; i++) {
                // 1. first add a sectionHeader
                meals.add(new MealNew(sectionHeaderNames[i]));
                // 2. then start adding drinks that go under that sectionHeader
                int nrOfItemsInCurrentCategory = nrOfItemsInSectionHeaders[i]; // e.g. we have 4 drinks in the "Soft drinks" sectionHeader
                for (int j = 0; j < nrOfItemsInCurrentCategory; j++) { // go through each soft drink
                    meals.add(new MealNew(String.valueOf(n), namesEst[n], "", namesEng[n], "", drinkQuantity[n], prices[n]));
                    n++;
                }
            }

        } else if (category.equals("Menüü")) {

            sectionHeaderNames = resources.getStringArray(R.array.maincourses_sectionheaders);
            nrOfItemsInSectionHeaders = resources.getIntArray(R.array.nr_of_items_in_maincourses_sectionheaders);

            IDs = resources.getStringArray(R.array.maincourse_ids);
            namesEst = resources.getStringArray(R.array.maincourse_est);
            namesRus = resources.getStringArray(R.array.maincourse_rus);
            namesEng = resources.getStringArray(R.array.maincourse_eng);
            namesCh = resources.getStringArray(R.array.maincourse_ch);
            prices = resources.getStringArray(R.array.maincourse_prices);

            int n = 0; // main course dish we are currently on
            for (int i = 0; i < sectionHeaderNames.length; i++) {
                // 1. first add a sectionHeader
                meals.add(new MealNew(sectionHeaderNames[i]));
                // 2. then start adding dishes that go under that sectionHeader
                int nrOfItemsInCurrentCategory = nrOfItemsInSectionHeaders[i]; // e.g. we have 4 dishes in the "Starters" sectionHeader
                for (int j = 0; j < nrOfItemsInCurrentCategory; j++) { // go through each starter
                    meals.add(new MealNew(IDs[n], namesEst[n], namesRus[n], namesEng[n], namesCh[n], "", prices[n]));
                    n++;
                }
            }

        } else if (category.equals("Magustoidud")) {

            IDs = resources.getStringArray(R.array.dessert_ids);
            namesEst = resources.getStringArray(R.array.dessert_est);
            namesRus = resources.getStringArray(R.array.dessert_rus);
            namesEng = resources.getStringArray(R.array.dessert_eng);
            namesCh = resources.getStringArray(R.array.dessert_ch);
            prices = resources.getStringArray(R.array.dessert_prices);

            for (int i = 0; i < namesEst.length; i++) {
                meals.add(new MealNew(IDs[i], namesEst[i], namesRus[i], namesEng[i], namesCh[i], "", prices[i]));
            }

        }

    }

    @Override
    public int getItemViewType(int position) {
        return meals.get(position).isSectionHeader() ? SEPARATOR : ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return meals.size();
    }

    @Override
    public Object getItem(int position) {
        return meals.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        ViewHolderNew viewHolder = null;

        int rowType = getItemViewType(position);

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            switch (rowType) {
                case ITEM:
                    row = inflater.inflate(R.layout.level2_activity_single_meal_new, parent, false);
                    break;
                case SEPARATOR:
                    row = inflater.inflate(R.layout.level2_activity_section_header_new, parent, false);
                    break;
            }

            viewHolder = new ViewHolderNew(row);
            row.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolderNew) row.getTag();
        }

        // we've inflated the views, now let's set texts for our textviews

        MealNew meal = meals.get(position);
        if (meal.isSectionHeader()) {

            viewHolder.getSectionHeader().setText(meal.getSectionHeaderName());

        } else {

            if (category.equals("Joogid")) {

                viewHolder.getFirstRow().setText(meal.getNameEst());
                // if there is no english name
                if (meal.getNameEng().equals("-")) {
                    viewHolder.getSecondRow().setVisibility(View.GONE);
                } else {
                    viewHolder.getSecondRow().setVisibility(View.VISIBLE);
                    viewHolder.getSecondRow().setText(meal.getNameEng());
                }
                // if there is no quantity of a drink
                if (meal.getQuantity().equals("-")) {
                    viewHolder.getThirdRow().setVisibility(View.GONE);
                } else {
                    viewHolder.getThirdRow().setVisibility(View.VISIBLE);
                    viewHolder.getThirdRow().setText(meal.getQuantity());
                }
                viewHolder.getFourthRow().setVisibility(View.GONE);
                viewHolder.getPrice().setText(meal.getPrice());

            } else { // main courses and desserts

                viewHolder.getFirstRow().setText(meal.getID() + ". " + meal.getNameEst());
                viewHolder.getSecondRow().setText(meal.getNameRus());
                viewHolder.getThirdRow().setText(meal.getNameEng());
                viewHolder.getFourthRow().setText(meal.getNameCh());
                viewHolder.getPrice().setText(meal.getPrice());

            }

        }

        return row;

    }

}