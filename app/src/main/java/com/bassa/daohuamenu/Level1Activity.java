package com.bassa.daohuamenu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Level1Activity extends AppCompatActivity {

    private Button drinksBtn, mainCoursesBtn, dessertsBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level1);

        drinksBtn = (Button) findViewById(R.id.drinksBtn);
        mainCoursesBtn = (Button) findViewById(R.id.mainCoursesBtn);
        dessertsBtn = (Button) findViewById(R.id.dessertsBtn);

    }

    public void openLevel2Activity(View view) {

        Intent level2Intent = new Intent(this, Level2ActivityNew.class);

        String categoryName = view.getTag().toString();
        level2Intent.putExtra("category", categoryName);

        startActivity(level2Intent);

    }

}
