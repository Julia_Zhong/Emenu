package com.bassa.daohuamenu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Level3Activity extends AppCompatActivity {

    private ImageView mealImage;
    private TextView mealName, mealDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level3);

        mealImage = (ImageView) findViewById(R.id.mealImage);
        mealName = (TextView) findViewById(R.id.mealName);
        mealDescription = (TextView) findViewById(R.id.mealDescription);

        Intent level3Intent = getIntent();
        if (level3Intent != null) {

            int imageId = level3Intent.getIntExtra("imageId", R.drawable.meal_1);
            String name = level3Intent.getStringExtra("name");
            String description = level3Intent.getStringExtra("description");

            mealImage.setImageResource(imageId);
            mealName.setText(name);
            mealDescription.setText(description);

        }

    }

}
